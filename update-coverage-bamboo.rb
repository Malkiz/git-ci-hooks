#!/usr/bin/env ruby

# git update hook for asserting code coverage of a ref being merged into a protected 
# ref (e.g. master) is the same or better
#
# requires Ruby 1.9.3+ 

require_relative 'ci-util'
require 'rexml/document'

include REXML

# Determine the code coverage for a particular commit by parsing clover artifacts
def find_coverage(bamboo, commit)
    # grab the clover.xml artifact from the build (assumes a shared artifact named 
    # 'clover' with 'clover.xml' at the root)
    clover_xml = shared_artifact_for_commit(bamboo, commit, bamboo["coverage_key"], "clover/clover.xml")
    doc = Document.new clover_xml

    # parse out the project metrics element from the response
    metrics = XPath.first(doc, "coverage/project/metrics")

    # Use algorithm similar to Clover (https://confluence.atlassian.com/x/LoHEB) for 
    # determining coverage percentage
    covered_elements = metrics.attribute("coveredconditionals").value.to_i
    covered_elements += metrics.attribute("coveredmethods").value.to_i
    covered_elements += metrics.attribute("coveredstatements").value.to_i

    elements = metrics.attribute("conditionals").value.to_i
    elements += metrics.attribute("methods").value.to_i
    elements += metrics.attribute("statements").value.to_i

    coverage = 0
    if (elements > 0)
        coverage = covered_elements / elements
    end 
    coverage
end

# parse args supplied by git: <ref_name> <old_sha> <new_sha>
ref = simple_branch_name ARGV[0]
prevCommit = ARGV[1]
newCommit = ARGV[2]

# test if the updated ref is one we want to enforce green builds for
exit_if_not_protected_ref(ref)

# get the tip of the most recently merged branch
tip_of_merged_branch = find_newest_non_merge_commit(prevCommit, newCommit)

# parse our bamboo server config
bamboo = read_config("bamboo", ["url", "username", "password", "coverage_key"])

# calculate code coverage for the old and new commits
prev_coverage = find_coverage(bamboo, prevCommit)
new_coverage = find_coverage(bamboo, tip_of_merged_branch)

# if the coverage has dropped for the new commit, block the update
if prev_coverage > new_coverage
    abort "Code coverage for #{shortSha(tip_of_merged_branch)} is only #{new_coverage}! #{ref} is currently at #{prev_coverage}." 
else 
    # if the coverage has increased, TFCIT
    puts "Nice work! Code coverage for #{ref} has increased by #{new_coverage - prev_coverage}."
end
